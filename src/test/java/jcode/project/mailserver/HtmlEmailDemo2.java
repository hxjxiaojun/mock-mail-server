package jcode.project.mailserver;

import com.mashape.unirest.http.Unirest;
import jcode.project.base.JSONUtils;


public class HtmlEmailDemo2 {

    public static class Email {

        private String from;
        private String[] to;
        private String subject;
        private String content;

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String[] getTo() {
            return to;
        }

        public void setTo(String[] to) {
            this.to = to;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static void main(String[] args) throws Exception {
        StringBuilder builder = new StringBuilder();
        // 测试10MB大小的邮件
        for(int i = 0; i<1024 * 1024 * 10; i++){
            builder.append('a');
        }

        Email mail = new Email();
        mail.setContent(builder.toString());
        mail.setSubject("邮件主题");
        mail.setFrom("user@126.com");
        mail.setTo(new String[]{"to1@126.com", "to2@126.com"});
        System.out.println(JSONUtils.toJSON(mail));

        String result = Unirest.post("http://localhost:9000/mail/send")
                .header("Content-type", "application/json; charset=utf-8")
                .body(JSONUtils.toJSON(mail))
                .asString()
                .getBody();
        System.out.println(result);

    }

}

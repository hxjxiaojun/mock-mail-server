package jcode.project.mailserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.Unirest;


public class HtmlEmailDemo {

    public static class Email {

        private String from;
        private String[] to;
        private String subject;
        private String content;

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String[] getTo() {
            return to;
        }

        public void setTo(String[] to) {
            this.to = to;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static void main(String[] args) throws Exception {
        Email mail = new Email();
        mail.setContent("<html>邮件内容,<button>按钮</button><html>");
        mail.setSubject("邮件主题");
        mail.setFrom("user@126.com");
        mail.setTo(new String[]{"to1@126.com", "to2@126.com"});

        String result = Unirest.post("http://localhost:9000/mail/send")
                .header("Content-type", "application/json; charset=utf-8")
                .body(new ObjectMapper().writeValueAsString(mail))
                .asString()
                .getBody();
        System.out.println(result);

    }

}

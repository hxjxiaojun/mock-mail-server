package jcode.project.mailserver.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import jcode.project.mailserver.entity.Mail;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MailMapper extends BaseMapper<Mail> {
}